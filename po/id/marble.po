# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the marble package.
# Linerly <linerly@protonmail.com>, 2022.
# Wantoyèk <wantoyek@gmail.com>, 2022.
#
msgid ""
msgstr ""
"Project-Id-Version: marble\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-04-26 00:45+0000\n"
"PO-Revision-Date: 2022-06-11 22:03+0700\n"
"Last-Translator: Linerly <linerly@protonmail.com>\n"
"Language-Team: Indonesian <kde-i18n-doc@kde.org>\n"
"Language: id\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 21.12.3\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Linerly"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "linerly@protonmail.com"

#: kdemain.cpp:91
#, kde-format
msgid "Marble Virtual Globe"
msgstr "Globe Virtual Marble"

#: kdemain.cpp:93
#, kde-format
msgid "A World Atlas."
msgstr "Sebuah Atlas Dunia."

#: kdemain.cpp:95
#, kde-format
msgid "(c) 2007-%1"
msgstr "(c) 2007-%1"

#: kdemain.cpp:100 kdemain.cpp:286
#, kde-format
msgid "Torsten Rahn"
msgstr "Torsten Rahn"

#: kdemain.cpp:101
#, kde-format
msgid "Developer and Original Author"
msgstr "Pengembang dan Penulis Asli"

#: kdemain.cpp:103
#, kde-format
msgid "Bernhard Beschow"
msgstr "Bernhard Beschow"

#: kdemain.cpp:104
#, kde-format
msgid "WMS Support, Mobile, Performance"
msgstr "Dukungan WMS, Ponsel, Performa"

#: kdemain.cpp:106
#, kde-format
msgid "Thibaut Gridel"
msgstr "Thibaut Gridel"

#: kdemain.cpp:107
#, kde-format
msgid "Geodata"
msgstr "Data Geografis"

#: kdemain.cpp:109
#, kde-format
msgid "Jens-Michael Hoffmann"
msgstr "Jens-Michael Hoffmann"

#: kdemain.cpp:110
#, kde-format
msgid "OpenStreetMap Integration, OSM Namefinder, Download Management"
msgstr "Integrasi OpenStreetMap, Pencari Nama OSM, Pengelola Unduhan"

#: kdemain.cpp:112
#, kde-format
msgid "Florian E&szlig;er"
msgstr "Florian E&szlig;er"

#: kdemain.cpp:113
#, kde-format
msgid "Elevation Profile"
msgstr "Profil Ketinggian"

#: kdemain.cpp:115
#, kde-format
msgid "Wes Hardaker"
msgstr "Wes Hardaker"

#: kdemain.cpp:116
#, kde-format
msgid "APRS Plugin"
msgstr "Plugin APRS"

#: kdemain.cpp:118 kdemain.cpp:202
#, kde-format
msgid "Bastian Holst"
msgstr "Bastian Holst"

#: kdemain.cpp:119
#, kde-format
msgid "Online Services support"
msgstr "Dukungan Layanan Daring"

#: kdemain.cpp:121 kdemain.cpp:172
#, kde-format
msgid "Guillaume Martres"
msgstr "Guillaume Martres"

#: kdemain.cpp:122
#, kde-format
msgid "Satellites"
msgstr "Satelit"

#: kdemain.cpp:124 kdemain.cpp:168
#, kde-format
msgid "Rene Kuettner"
msgstr "Rene Kuettner"

#: kdemain.cpp:125
#, kde-format
msgid "Satellites, Eclipses"
msgstr "Satelit, Gerhana"

#: kdemain.cpp:127
#, kde-format
msgid "Friedrich W. H. Kossebau"
msgstr "Friedrich W. H. Kossebau"

#: kdemain.cpp:128
#, kde-format
msgid "Plasma Integration, Bugfixes"
msgstr "Integrasi Plasma, Perbaikan Kutu"

#: kdemain.cpp:130
#, kde-format
msgid "Dennis Nienhüser"
msgstr "Dennis Nienhüser"

#: kdemain.cpp:131
#, kde-format
msgid "Routing, Navigation, Mobile"
msgstr "Perutean, Navigasi, Ponsel"

#: kdemain.cpp:133
#, kde-format
msgid "Niko Sams"
msgstr "Niko Sams"

#: kdemain.cpp:134
#, kde-format
msgid "Routing, Elevation Profile"
msgstr "Perutean, Profil Ketinggian"

#: kdemain.cpp:136 kdemain.cpp:206
#, kde-format
msgid "Patrick Spendrin"
msgstr "Patrick Spendrin"

#: kdemain.cpp:137
#, kde-format
msgid "Core Developer: KML and Windows support"
msgstr "Pengembang Inti: Dukungan KML dan Windows"

#: kdemain.cpp:139
#, kde-format
msgid "Eckhart Wörner"
msgstr "Eckhart Wörner"

#: kdemain.cpp:140
#, kde-format
msgid "Bugfixes"
msgstr "Perbaikan Kutu"

#: kdemain.cpp:145
#, kde-format
msgid "Inge Wallin"
msgstr "Inge Wallin"

#: kdemain.cpp:146
#, kde-format
msgid "Core Developer and Co-Maintainer"
msgstr "Pengembang Inti dan Bantuan Pemeliharaan"

#: kdemain.cpp:148
#, kde-format
msgid "Henry de Valence"
msgstr "Henry de Valence"

#: kdemain.cpp:149
#, kde-format
msgid "Core Developer: Marble Runners, World-Clock Plasmoid"
msgstr "Pengembang Inti: Runner Marble, Plasmoid Jam Dunia"

#: kdemain.cpp:151
#, kde-format
msgid "Pino Toscano"
msgstr "Pino Toscano"

#: kdemain.cpp:152
#, kde-format
msgid "Network plugins"
msgstr "Plugin jaringan"

#: kdemain.cpp:154
#, kde-format
msgid "Harshit Jain"
msgstr "Harshit Jain"

#: kdemain.cpp:155
#, kde-format
msgid "Planet filter"
msgstr "Penyaring planet"

#: kdemain.cpp:157
#, kde-format
msgid "Simon Edwards"
msgstr "Simon Edwards"

#: kdemain.cpp:158
#, kde-format
msgid "Marble Python Bindings"
msgstr "Pengikat Python Marble"

#: kdemain.cpp:160
#, kde-format
msgid "Magnus Valle"
msgstr "Magnus Valle"

#: kdemain.cpp:161
#, kde-format
msgid "Historical Maps"
msgstr "Peta Bersejarah"

#: kdemain.cpp:163
#, kde-format
msgid "Médéric Boquien"
msgstr "Médéric Boquien"

#: kdemain.cpp:164
#, kde-format
msgid "Astronomical Observatories"
msgstr "Observatorium Astronomi"

#: kdemain.cpp:169
#, kde-format
msgid ""
"ESA Summer of Code in Space 2012 Project: Visualization of planetary "
"satellites"
msgstr "Proyek ESA Summer of Code in Space 2012: Visualisasi satelit planet"

#: kdemain.cpp:173
#, kde-format
msgid ""
"ESA Summer of Code in Space 2011 Project: Visualization of Satellite Orbits"
msgstr "Proyek ESA Summer of Code in Space: Visualisasi Orbit Satelit"

#: kdemain.cpp:178
#, kde-format
msgid "Konstantin Oblaukhov"
msgstr "Konstantin Oblaukhov"

#: kdemain.cpp:179
#, kde-format
msgid "Google Summer of Code 2011 Project: OpenStreetMap Vector Rendering"
msgstr "Proyek Google Summer of Code 2011: Perenderan Vektor OpenStreetMap"

#: kdemain.cpp:182
#, kde-format
msgid "Daniel Marth"
msgstr "Daniel Marth"

#: kdemain.cpp:183
#, kde-format
msgid "Google Summer of Code 2011 Project: Marble Touch on MeeGo"
msgstr "Proyek Google Summer of Code 2011: Marble Touch di MeeGo"

#: kdemain.cpp:186
#, kde-format
msgid "Gaurav Gupta"
msgstr "Gaurav Gupta"

#: kdemain.cpp:187
#, kde-format
msgid "Google Summer of Code 2010 Project: Bookmarks"
msgstr "Proyek Google Summer of Code 2010: Markah"

#: kdemain.cpp:190
#, kde-format
msgid "Harshit Jain "
msgstr "Harshit Jain"

#: kdemain.cpp:191
#, kde-format
msgid "Google Summer of Code 2010 Project: Time Support"
msgstr "Proyek Google Summer of Code 2010: Dukungan Waktu"

#: kdemain.cpp:194
#, kde-format
msgid "Siddharth Srivastava"
msgstr "Siddharth Srivastava"

#: kdemain.cpp:195
#, kde-format
msgid "Google Summer of Code 2010 Project: Turn-by-turn Navigation"
msgstr "Proyek Google Summer of Code 2010: Navigasi Belokan demi Belokan"

#: kdemain.cpp:198 kdemain.cpp:218
#, kde-format
msgid "Andrew Manson"
msgstr "Andrew Manson"

#: kdemain.cpp:199
#, kde-format
msgid "Google Summer of Code 2009 Project: OSM Annotation"
msgstr "Proyek Google Summer of Code 2009: Anotasi OSM"

#: kdemain.cpp:203
#, kde-format
msgid "Google Summer of Code 2009 Project: Online Services"
msgstr "Proyek Google Summer of Code 2009: Layanan Daring"

#: kdemain.cpp:207
#, kde-format
msgid "Google Summer of Code 2008 Project: Vector Tiles for Marble"
msgstr "Proyek Google Summer of Code 2008: Ubin Vektor untuk Marble"

#: kdemain.cpp:210
#, kde-format
msgid "Shashank Singh"
msgstr "Shashank Singh"

#: kdemain.cpp:211
#, kde-format
msgid ""
"Google Summer of Code 2008 Project: Panoramio / Wikipedia -photo support for "
"Marble"
msgstr ""
"Proyek Google Summer of Code 2008: Dukungan foto Panoramio / Wikipedia untuk "
"Marble"

#: kdemain.cpp:214
#, kde-format
msgid "Carlos Licea"
msgstr "Carlos Licea"

#: kdemain.cpp:215
#, kde-format
msgid ""
"Google Summer of Code 2007 Project: Equirectangular Projection (\"Flat Map\")"
msgstr ""
"Proyek Google Summer of Code: Proyeksi Persegi Panjang (\"Peta Datar\")"

#: kdemain.cpp:219
#, kde-format
msgid "Google Summer of Code 2007 Project: GPS Support for Marble"
msgstr "Proyek Google Summer of Code 2007: Dukungan GPS untuk Marble"

#: kdemain.cpp:222
#, kde-format
msgid "Murad Tagirov"
msgstr "Murad Tagirov"

#: kdemain.cpp:223
#, kde-format
msgid "Google Summer of Code 2007 Project: KML Support for Marble"
msgstr "Proyek Google Summer of Code 2007: Dukungan KML untuk Marble"

#: kdemain.cpp:228
#, kde-format
msgid "Simon Schmeisser"
msgstr "Simon Schmeisser"

#: kdemain.cpp:229 kdemain.cpp:231 kdemain.cpp:233 kdemain.cpp:235
#: kdemain.cpp:237 kdemain.cpp:239 kdemain.cpp:241 kdemain.cpp:243
#: kdemain.cpp:245 kdemain.cpp:247 kdemain.cpp:249 kdemain.cpp:251
#: kdemain.cpp:253 kdemain.cpp:255 kdemain.cpp:257 kdemain.cpp:259
#: kdemain.cpp:261 kdemain.cpp:263 kdemain.cpp:265
#, kde-format
msgid "Development & Patches"
msgstr "Pengembangan & Tambalan"

#: kdemain.cpp:230
#, kde-format
msgid "Claudiu Covaci"
msgstr "Claudiu Covaci"

#: kdemain.cpp:232
#, kde-format
msgid "David Roberts"
msgstr "David Roberts"

#: kdemain.cpp:234
#, kde-format
msgid "Nikolas Zimmermann"
msgstr "Nikolas Zimmermann"

#: kdemain.cpp:236
#, kde-format
msgid "Jan Becker"
msgstr "Jan Becker"

#: kdemain.cpp:238
#, kde-format
msgid "Stefan Asserhäll"
msgstr "Stefan Asserhäll"

#: kdemain.cpp:240
#, kde-format
msgid "Laurent Montel"
msgstr "Laurent Montel"

#: kdemain.cpp:242
#, kde-format
msgid "Mayank Madan"
msgstr "Mayank Madan"

#: kdemain.cpp:244
#, kde-format
msgid "Prashanth Udupa"
msgstr "Prashanth Udupa"

#: kdemain.cpp:246
#, kde-format
msgid "Anne-Marie Mahfouf"
msgstr "Anne-Marie Mahfouf"

#: kdemain.cpp:248
#, kde-format
msgid "Josef Spillner"
msgstr "Josef Spillner"

#: kdemain.cpp:250
#, kde-format
msgid "Frerich Raabe"
msgstr "Frerich Raabe"

#: kdemain.cpp:252
#, kde-format
msgid "Frederik Gladhorn"
msgstr "Frederik Gladhorn"

#: kdemain.cpp:254
#, kde-format
msgid "Fredrik Höglund"
msgstr "Fredrik Höglund"

#: kdemain.cpp:256
#, kde-format
msgid "Albert Astals Cid"
msgstr "Albert Astals Cid"

#: kdemain.cpp:258
#, kde-format
msgid "Thomas Zander"
msgstr "Thomas Zander"

#: kdemain.cpp:260
#, kde-format
msgid "Joseph Wenninger"
msgstr "Joseph Wenninger"

#: kdemain.cpp:262
#, kde-format
msgid "Kris Thomsen"
msgstr "Kris Thomsen"

#: kdemain.cpp:264
#, kde-format
msgid "Daniel Molkentin"
msgstr "Daniel Molkentin"

#: kdemain.cpp:266
#, kde-format
msgid "Christophe Leske"
msgstr "Cristophe Leske"

#: kdemain.cpp:267 kdemain.cpp:269 kdemain.cpp:271 kdemain.cpp:273
#: kdemain.cpp:275 kdemain.cpp:277 kdemain.cpp:279 kdemain.cpp:281
#: kdemain.cpp:283
#, kde-format
msgid "Platforms & Distributions"
msgstr "Platform & Distribusi"

#: kdemain.cpp:268
#, kde-format
msgid "Sebastian Wiedenroth"
msgstr "Sebastian Wiedenroth"

#: kdemain.cpp:270
#, kde-format
msgid "Tim Sutton"
msgstr "Tim Sutton"

#: kdemain.cpp:272
#, kde-format
msgid "Christian Ehrlicher"
msgstr "Christian Ehrlicher"

#: kdemain.cpp:274
#, kde-format
msgid "Ralf Habacker"
msgstr "Ralf Habacker"

#: kdemain.cpp:276
#, kde-format
msgid "Steffen Joeris"
msgstr "Steffen Joeris"

#: kdemain.cpp:278
#, kde-format
msgid "Marcus Czeslinski"
msgstr "Marcus Czeslinski"

#: kdemain.cpp:280
#, kde-format
msgid "Marcus D. Hanwell"
msgstr "Marcus D. Hanwell"

#: kdemain.cpp:282
#, kde-format
msgid "Chitlesh Goorah"
msgstr "Chitlesh Goorah"

#: kdemain.cpp:284
#, kde-format
msgid "Nuno Pinheiro"
msgstr "Nuno Pinheiro"

#: kdemain.cpp:285 kdemain.cpp:287
#, kde-format
msgid "Artwork"
msgstr "Lukisan"

#: kdemain.cpp:290
#, kde-format
msgid "Luis Silva"
msgstr "Luis Silva"

#: kdemain.cpp:291 kdemain.cpp:293 kdemain.cpp:295 kdemain.cpp:297
#: kdemain.cpp:299 kdemain.cpp:301 kdemain.cpp:303 kdemain.cpp:305
#: kdemain.cpp:307 kdemain.cpp:309
#, kde-format
msgid "Various Suggestions & Testing"
msgstr "Banyak Saran & Percobaan"

#: kdemain.cpp:292
#, kde-format
msgid "Stefan Jordan"
msgstr "Stefan Jordan"

#: kdemain.cpp:294
#, kde-format
msgid "Robert Scott"
msgstr "Robert Scott"

#: kdemain.cpp:296
#, kde-format
msgid "Lubos Petrovic"
msgstr "Lubos Petrovic"

#: kdemain.cpp:298
#, kde-format
msgid "Benoit Sigoure"
msgstr "Benoit Sigoure"

#: kdemain.cpp:300
#, kde-format
msgid "Martin Konold"
msgstr "Martin Konold"

#: kdemain.cpp:302
#, kde-format
msgid "Matthias Welwarsky"
msgstr "Matthias Welwarsky"

#: kdemain.cpp:304
#, kde-format
msgid "Rainer Endres"
msgstr "Rainer Endres"

#: kdemain.cpp:306
#, kde-format
msgid "Ralf Gesellensetter"
msgstr "Ralf Gesellensetter"

#: kdemain.cpp:308
#, kde-format
msgid "Tim Alder"
msgstr "Tim Alder"

#: kdemain.cpp:310
#, kde-format
msgid "John Layt"
msgstr "John Layt"

#: kdemain.cpp:311
#, kde-format
msgid ""
"Special thanks for providing an important source of inspiration by creating "
"Marble's predecessor \"Kartographer\"."
msgstr ""
"Terima kasih yang spesial untuk menyediakan sumber inspirasi yang penting "
"dengan membuat pendahulu Marble, \"Kartographer\"."

#: kdemain.cpp:327
#, kde-format
msgid "Enable debug output"
msgstr "Aktifkan keluaran pengawakutu"

#: kdemain.cpp:329
#, kde-format
msgid "Display OSM placemarks according to the level selected"
msgstr "Tampilkan tanda tempat OSM berdasarkan tingkat yang dipilih"

#: kdemain.cpp:331
#, kde-format
msgid "Make a time measurement to check performance"
msgstr "Buat pengukuran waktu untuk memeriksa performa"

#: kdemain.cpp:333
#, kde-format
msgid "Show frame rate"
msgstr "Tampilkan kecepatan frame"

#: kdemain.cpp:335
#, kde-format
msgid "Show tile IDs"
msgstr "Tampilkan ID ubin"

#: kdemain.cpp:337
#, kde-format
msgid "Show time spent in each layer"
msgstr "Tampilkan waktu yang dihabiskan dalam setiap lapisan"

#: kdemain.cpp:339
#, kde-format
msgid "Use a different directory <directory> which contains map data."
msgstr "Gunakan direktori <directory> yang berbeda, yang berisi data peta."

#: kdemain.cpp:341
#, kde-format
msgid "Do not use the interface optimized for small screens"
msgstr "Jangan gunakan antarmuka yang dioptimalkan untuk perangkat ponsel"

#: kdemain.cpp:342
#, kde-format
msgid "Use the interface optimized for small screens"
msgstr "Gunakan antarmuka yang dioptimalkan untuk layar kecil"

#: kdemain.cpp:344
#, kde-format
msgid "Do not use the interface optimized for high resolutions"
msgstr "Jangan gunakan antarmuka yang dioptimalkan untuk resolusi tinggi"

#: kdemain.cpp:345
#, kde-format
msgid "Use the interface optimized for high resolutions"
msgstr "Gunakan antarmuka yang dioptimalkan untuk resolusi tinggi"

#: kdemain.cpp:347
#, kde-format
msgid "Show map at given lat lon <coordinates>"
msgstr "Tampilkan peta pada garis lintas dan garis bujur <coordinates>"

#: kdemain.cpp:349
#, kde-format
msgid "Show map at given geo <uri>"
msgstr "Tampilkan pada geo <uri>"

#: kdemain.cpp:351
#, kde-format
msgid "Set the distance of the observer to the globe (in km)"
msgstr "Tetapkan jarak observer ke globe (dalam km)"

#: kdemain.cpp:353
#, kde-format
msgid "Use map <id> (e.g. \"earth/openstreetmap/openstreetmap.dgml\")"
msgstr "Gunakan peta <id> (mis. \"earth/openstreetmap/openstreetmap.dgml\")"

#: kdemain.cpp:355
#, kde-format
msgid "One or more placemark files to be opened"
msgstr "Satu file markah tempat atau lebih yang akan dibuka"

#. i18n: ectx: label, entry, group (Time)
#: marble.kcfg:58
#, kde-format
msgid "The date and time of marble clock"
msgstr "Tanggal dan waktu jam marble"

#. i18n: ectx: label, entry, group (Time)
#: marble.kcfg:61
#, kde-format
msgid "The speed of marble clock"
msgstr "Kecepatan jam marble"

#. i18n: ectx: label, entry, group (View)
#: marble.kcfg:107
#, kde-format
msgid "The unit chosen to measure distances."
msgstr "Unit yang dipilih untuk mengukur jarak."

#. i18n: ectx: label, entry, group (View)
#: marble.kcfg:115
#, kde-format
msgid "The unit chosen to measure angles."
msgstr "Unit yang dipilih untuk mengukur sudut."

#. i18n: ectx: label, entry, group (View)
#: marble.kcfg:124
#, kde-format
msgid "The quality at which a still map gets painted."
msgstr "Kualitas peta yang akan digambarkan."

#. i18n: ectx: label, entry, group (View)
#: marble.kcfg:135
#, kde-format
msgid "The quality at which an animated map gets painted."
msgstr "Kualitas peta beranimasi yang akan digambarkan."

#. i18n: ectx: label, entry, group (View)
#: marble.kcfg:146
#, kde-format
msgid "The localization of the labels."
msgstr "Lokalisasi label."

#. i18n: ectx: label, entry (mapFont), group (View)
#: marble.kcfg:155
#, kde-format
msgid "The general font used on the map."
msgstr "Font umum yang digunakan pada peta."

#. i18n: ectx: label, entry (lastFileOpenDir), group (View)
#: marble.kcfg:159
#, kde-format
msgid "The last directory that was opened by File->Open."
msgstr "Direktori terakhir kali yang dibuka dengan File->Buka."

#. i18n: ectx: label, entry, group (Navigation)
#: marble.kcfg:179
#, kde-format
msgid "The behaviour of the planet's axis on mouse dragging."
msgstr "Tingkah laku axis planet pada seretan mouse"

#. i18n: ectx: label, entry, group (Navigation)
#: marble.kcfg:187
#, kde-format
msgid "The location shown on application startup."
msgstr "Lokasi yang ditampilkan pada pemulaian aplikasi."

#. i18n: ectx: label, entry, group (Navigation)
#: marble.kcfg:201
#, kde-format
msgid "Display animation on voyage to target."
msgstr "Tampilkan animasi pada perjalanan ke sasaran."

#. i18n: ectx: label, entry, group (Navigation)
#: marble.kcfg:205
#, kde-format
msgid "The external OpenStreetMap editor application"
msgstr "Aplikasi editor OpenStreetMap eksternal"

#. i18n: ectx: label, entry, group (Cache)
#: marble.kcfg:211
#, kde-format
msgid "Cache for tiles reserved in the physical memory."
msgstr "Cache untuk ubin disimpan di memori fisik."

#. i18n: ectx: label, entry, group (Cache)
#: marble.kcfg:217
#, kde-format
msgid "Maximum space on the hard disk that can be used to store tiles."
msgstr ""
"Maksimum ruang pada hard disk yang dapat digunakan untuk menyimpan ubin."

#. i18n: ectx: label, entry (proxyUrl), group (Cache)
#: marble.kcfg:223
#, kde-format
msgid "URL for the proxy server."
msgstr "URL untuk server proxy."

#. i18n: ectx: label, entry, group (Cache)
#: marble.kcfg:227
#, kde-format
msgid "Port for the proxy server."
msgstr "Port untuk server proxy."

#. i18n: ectx: label, entry (proxyUser), group (Cache)
#: marble.kcfg:233
#, kde-format
msgid "Username for authorization."
msgstr "Nama pengguna untuk otorisasi."

#. i18n: ectx: label, entry (proxyPass), group (Cache)
#: marble.kcfg:236
#, kde-format
msgid "Password for authorization."
msgstr "Kata sandi untuk otorisasi."

#. i18n: ectx: label, entry (proxyHttp), group (Cache)
#: marble.kcfg:239
#, kde-format
msgid "Proxy type is HTTP"
msgstr "Tipe proxy adalah HTTP"

#. i18n: ectx: label, entry (proxySocks5), group (Cache)
#: marble.kcfg:250
#, kde-format
msgid "Proxy type is Socks5"
msgstr "Tipe proxy adalah Socks5"

#. i18n: ectx: label, entry (proxyAuth), group (Cache)
#: marble.kcfg:254
#, kde-format
msgid "Proxy requires Authentication"
msgstr "Proxy membutuhkan otentikasi"

#. i18n: ectx: label, entry (activePositionTrackingPlugin), group (Plugins)
#: marble.kcfg:260
#, kde-format
msgid "The position tracking plugin used to determine the current location"
msgstr ""
"Plugin pelacakan posisi yang digunakan untuk menentukan lokasi saat ini"

#: marble_part.cpp:102
#, kde-format
msgid "Position: %1"
msgstr "Posisi: %1"

#: marble_part.cpp:103
#, kde-format
msgid "Altitude: %1"
msgstr "Ketinggian: %1"

#: marble_part.cpp:104
#, kde-format
msgid "Tile Zoom Level: %1"
msgstr "Tingkat Perbesaran Ubin: %1"

#: marble_part.cpp:105
#, kde-format
msgid "Time: %1"
msgstr "Waktu: %1"

#: marble_part.cpp:167
#, kde-format
msgid ""
"Sorry, unable to open '%1':\n"
"'%2'"
msgstr ""
"Maaf, tidak dapat membuka '%1':\n"
"'%2'"

#: marble_part.cpp:168
#, kde-format
msgid "File not readable"
msgstr "File tidak dapat dibaca"

#: marble_part.cpp:214
#, kde-format
msgid "marble_part"
msgstr "bagian_marble"

#: marble_part.cpp:217
#, kde-format
msgid "A Virtual Globe"
msgstr "Sebuah Globe Virtual"

#: marble_part.cpp:231
#, kde-format
msgid "Sorry, unable to open '%1'. The file is not accessible."
msgstr "Maaf, tidak dapat membuka '%1'. Filenya tidak dapat diakses."

#: marble_part.cpp:232
#, kde-format
msgid "File not accessible"
msgstr "File tidak dapat diakses"

#: marble_part.cpp:253
#, kde-format
msgid "All Supported Files"
msgstr "Semua File Yang Didukung"

#: marble_part.cpp:259
#, kde-format
msgctxt "@title:window"
msgid "Open File"
msgstr "Buka File"

#: marble_part.cpp:276
#, kde-format
msgctxt "@title:window"
msgid "Export Map"
msgstr "Ekspor Peta"

#: marble_part.cpp:277
#, kde-format
msgid "Images *.jpg *.png"
msgstr "Citra *.jpg *.png"

#: marble_part.cpp:291
#, kde-format
msgctxt "Application name"
msgid "Marble"
msgstr "Marble"

#: marble_part.cpp:292
#, kde-format
msgid "An error occurred while trying to save the file.\n"
msgstr "Sebuah kesalahan terjadi saat menyimpan file.\n"

#: marble_part.cpp:483
#, kde-format
msgid "Unnamed"
msgstr "Tidak dinamai"

#: marble_part.cpp:714
#, kde-format
msgctxt "Action for downloading an entire region of a map"
msgid "Download Region..."
msgstr "Unduh Wilayah..."

#: marble_part.cpp:728
#, kde-format
msgctxt "Action for saving the map to a file"
msgid "&Export Map..."
msgstr "&Ekspor Peta..."

#: marble_part.cpp:737
#, kde-format
msgctxt "Action for toggling offline mode"
msgid "&Work Offline"
msgstr "&Bekerja Secara Luring"

#: marble_part.cpp:747
#, kde-format
msgctxt "Action for copying the map to the clipboard"
msgid "&Copy Map"
msgstr "&Salin Peta"

#: marble_part.cpp:754
#, kde-format
msgctxt "Action for copying the coordinates to the clipboard"
msgid "C&opy Coordinates"
msgstr "S&alin Koordinat"

#: marble_part.cpp:762
#, kde-format
msgctxt "Action for opening a file"
msgid "&Open..."
msgstr "&Buka..."

#: marble_part.cpp:769
#, kde-format
msgctxt "Action for downloading maps (GHNS)"
msgid "Download Maps..."
msgstr "Unduh Peta..."

#: marble_part.cpp:772
#, kde-format
msgctxt "Status tip"
msgid "Download new maps"
msgstr "Unduh peta baru"

#: marble_part.cpp:778
#, kde-format
msgctxt "Action for creating new maps"
msgid "&Create a New Map..."
msgstr "&Buat Peta Dunia Baru..."

#: marble_part.cpp:783
#, kde-format
msgctxt "Status tip"
msgid "A wizard guides you through the creation of your own map theme."
msgstr "Sebuah pemandu akan memandu Anda untuk membuat tema peta Anda sendiri."

#: marble_part.cpp:810
#, kde-format
msgctxt "Action for toggling clouds"
msgid "&Clouds"
msgstr "&Awan"

#: marble_part.cpp:817
#, kde-format
msgctxt "Action for sun control dialog"
msgid "S&un Control..."
msgstr "Kontrol M&atahari..."

#: marble_part.cpp:827
#, kde-format
msgctxt "Action for time control dialog"
msgid "&Time Control..."
msgstr "Kontrol &Waktu..."

#: marble_part.cpp:836
#, kde-format
msgctxt "Action for locking float items on the map"
msgid "Lock Position"
msgstr "Kunci Posisi"

#: marble_part.cpp:847
#, kde-format
msgid "Show Shadow"
msgstr "Tampilkan Bayangan"

#: marble_part.cpp:850
#, kde-format
msgid "Hide Shadow"
msgstr "Sembunyikan Bayangan"

#: marble_part.cpp:851
#, kde-format
msgid "Shows and hides the shadow of the sun"
msgstr "Menampilkan dan menyembunyikan bayangan matahari"

#: marble_part.cpp:855
#, kde-format
msgid "Show sun icon on the Sub-Solar Point"
msgstr "Tampilkan ikon matahari di Titik Sub-Solar"

#: marble_part.cpp:857
#, kde-format
msgid "Hide sun icon on the Sub-Solar Point"
msgstr "Sembunyikan ikon matahari di Titik Sub-Solar"

#: marble_part.cpp:858
#, kde-format
msgid "Show sun icon on the sub-solar point"
msgstr "Tampilkan ikon matahari di titik sub-solar"

#: marble_part.cpp:863
#, kde-format
msgid "Lock Globe to the Sub-Solar Point"
msgstr "Kunci Globe pada Titik Sub-Solar"

#: marble_part.cpp:865
#, kde-format
msgid "Unlock Globe to the Sub-Solar Point"
msgstr "Buka Kunci Globe ke Titik Sub-Solar"

#: marble_part.cpp:866
#, kde-format
msgid "Lock globe to the sub-solar point"
msgstr "Kunci globe pada titik sub-solar"

#: marble_part.cpp:881
#, kde-format
msgctxt "Add Bookmark"
msgid "Add &Bookmark..."
msgstr "Tambahkan &Markah..."

#: marble_part.cpp:889
#, kde-format
msgctxt "Show Bookmarks"
msgid "Show &Bookmarks"
msgstr "Tampilkan &Markah"

#: marble_part.cpp:890
#, kde-format
msgid "Show or hide bookmarks in the map"
msgstr "Tampilkan atau sembunyikan markah di peta"

#: marble_part.cpp:898
#, kde-format
msgid "&Set Home Location"
msgstr "&Tetapkan Lokasi Rumah"

#: marble_part.cpp:905
#, kde-format
msgctxt "Manage Bookmarks"
msgid "&Manage Bookmarks..."
msgstr "&Kelola Markah..."

#: marble_part.cpp:917
#, kde-format
msgctxt "Edit the map in an external application"
msgid "&Edit Map..."
msgstr "&Edit Peta..."

#: marble_part.cpp:925
#, kde-format
msgid "&Record Movie"
msgstr "&Rekam Film"

#: marble_part.cpp:927
#, kde-format
msgid "Records a movie of the globe"
msgstr "Merekam sebuah film dari globe"

#: marble_part.cpp:933
#, kde-format
msgid "&Stop Recording"
msgstr "&Berhenti Merekam"

#: marble_part.cpp:935
#, kde-format
msgid "Stop recording a movie of the globe"
msgstr "Berhenti merekam sebuah film dari globe"

#: marble_part.cpp:1287
#, kde-format
msgctxt "Action for toggling"
msgid "Show Position"
msgstr "Tampilkan Posisi"

#: marble_part.cpp:1289
#, kde-format
msgctxt "Action for toggling"
msgid "Show Date and Time"
msgstr "Tampilkan Tanggal dan Waktu"

#: marble_part.cpp:1291
#, kde-format
msgctxt "Action for toggling"
msgid "Show Altitude"
msgstr "Tampilkan Ketinggian"

#: marble_part.cpp:1294
#, kde-format
msgctxt "Action for toggling"
msgid "Show Tile Zoom Level"
msgstr "Tampilkan Tingkat Perbesaran Ubin"

#: marble_part.cpp:1296
#, kde-format
msgctxt "Action for toggling"
msgid "Show Download Progress Bar"
msgstr "Tampilkan Bilah Progres Unduhan"

#: marble_part.cpp:1393
#, kde-format
msgid "View"
msgstr "Tampilan"

#: marble_part.cpp:1404
#, kde-format
msgid "Navigation"
msgstr "Navigasi"

#: marble_part.cpp:1415
#, kde-format
msgid "Cache & Proxy"
msgstr "Cache & Proxy"

#: marble_part.cpp:1428
#, kde-format
msgid "Date & Time"
msgstr "Tanggal & Waktu"

#: marble_part.cpp:1436
#, kde-format
msgid "Synchronization"
msgstr "Sinkronisasi"

#: marble_part.cpp:1449
#, kde-format
msgid "Routing"
msgstr "Perutean"

#: marble_part.cpp:1457
#, kde-format
msgid "Plugins"
msgstr "Plugin"

#. i18n: ectx: Menu (file)
#: marble_part.rc:6 marbleui.rc:5
#, kde-format
msgid "&File"
msgstr "&File"

#. i18n: ectx: Menu (edit)
#: marble_part.rc:21 marbleui.rc:14
#, kde-format
msgid "&Edit"
msgstr "&Edit"

#. i18n: ectx: Menu (view)
#: marble_part.rc:31 marbleui.rc:18
#, kde-format
msgid "&View"
msgstr "&Tampilan"

#. i18n: ectx: Menu (infoboxes)
#: marble_part.rc:36
#, kde-format
msgid "&Info Boxes"
msgstr "&Kotak Info"

#. i18n: ectx: Menu (onlineservices)
#: marble_part.rc:42
#, kde-format
msgid "&Online Services"
msgstr "&Layanan Daring"

#. i18n: ectx: Menu (settings)
#: marble_part.rc:55 marbleui.rc:31
#, kde-format
msgid "&Settings"
msgstr "&Pengaturan"

#. i18n: ectx: Menu (panels)
#: marble_part.rc:59
#, kde-format
msgid "&Panels"
msgstr "&Panel"

#. i18n: ectx: Menu (viewSize)
#: marble_part.rc:65
#, kde-format
msgid "&View Size"
msgstr "&Ukuran Tampilan"

#. i18n: ectx: Menu (bookmarks)
#: marble_part.rc:73 marbleui.rc:29
#, kde-format
msgid "&Bookmarks"
msgstr "&Markah"

#. i18n: ectx: ToolBar (mainToolBar)
#: marble_part.rc:84 marbleui.rc:36
#, kde-format
msgid "Main Toolbar"
msgstr "Bilah Alat Utama"

#. i18n: ectx: ToolBar (pluginToolBar)
#: marble_part.rc:97
#, kde-format
msgid "Edit Toolbar"
msgstr "Edit Bilah Alat"
