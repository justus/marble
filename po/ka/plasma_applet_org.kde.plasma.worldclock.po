# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the marble package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: marble\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2021-09-28 00:13+0000\n"
"PO-Revision-Date: 2022-08-27 04:58+0200\n"
"Last-Translator: Temuri Doghonadze <temuri.doghonadze@gmail.com>\n"
"Language-Team: Georgian <kde-i18n-doc@kde.org>\n"
"Language: ka\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Poedit 3.1.1\n"

#: package/contents/config/config.qml:12
#, kde-format
msgid "Map Display"
msgstr "რუკის ჩვენება"

#: package/contents/ui/configMapDisplay.qml:28
#, kde-format
msgid "Projection:"
msgstr "პროექცია:"

#: package/contents/ui/configMapDisplay.qml:36
#, kde-format
msgid "Equirectangular"
msgstr "თანასწორკუთხა"

#: package/contents/ui/configMapDisplay.qml:37
#, kde-format
msgid "Mercator"
msgstr "სავაჭრო"

#: package/contents/ui/configMapDisplay.qml:54
#, kde-format
msgid "Center on:"
msgstr "ცენტრირება:"

#: package/contents/ui/configMapDisplay.qml:62
#, kde-format
msgid "Daylight"
msgstr "დღის სინათლე"

#: package/contents/ui/configMapDisplay.qml:63
#, kde-format
msgid "Longitude"
msgstr "გრძედი"

#: package/contents/ui/configMapDisplay.qml:64
#, kde-format
msgid "Location"
msgstr "დებარეობა"

#: package/contents/ui/configMapDisplay.qml:82
#, kde-format
msgid "Longitude:"
msgstr "გეოლოკაციის გრძედი:"

#: package/contents/ui/configMapDisplay.qml:99
#, kde-format
msgid "Show date:"
msgstr "თარიღის ჩვენება:"

#: package/contents/ui/configTimeZones.qml:17
#, kde-format
msgid "Use custom time zone selection:"
msgstr "დროის სარტყლის ხელით არჩევა:"
